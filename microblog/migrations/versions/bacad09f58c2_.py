"""empty message

Revision ID: bacad09f58c2
Revises: 4b0a2856fb28
Create Date: 2019-06-22 17:01:47.413587

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'bacad09f58c2'
down_revision = '4b0a2856fb28'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('comment', sa.Column('body', sa.String(length=140), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('comment', 'body')
    # ### end Alembic commands ###
